package usage;

import model.Model;
import model.Project;
import model.SNodeRef;
import util.Computation;
import util.UniversalComputation;

public class UserCode {
    public void userCode() {
        Model model = Project.getModel();

        // Getting a computation for any specific "context" (here Object) is not going to get us anywhere.
        // There is no way to run such computation: methods on Computation only create more (unexecuted) computations.
        Computation<Object, SNodeRef<Object>> useless1 = model.getNodeById(42);

        // We can only run UniversalComputations.
        // For any previously specified type T, there is no way to create a UniversalComputation returning SNodeRef<T>.
        // Here is a failed attempt (for T = Object):
        UniversalComputation<SNodeRef<Object>> comp1 = new UniversalComputation<SNodeRef<Object>>() {
            @Override
            public <S> Computation<S, SNodeRef<Object>> getInstance() {
                // does not compile:
                // Incompatible types.
                //   Found:    Computation<S, SNodeRef<S>>
                //   Required: Computation<S, SNodeRef<Object>>
                return model.<S>getNodeById(42);
            }
        };

        // Nevertheless, SNodeRef can escape if we don't care about its type argument and let it be forgotten:
        UniversalComputation<SNodeRef<?>> comp2 = new UniversalComputation<SNodeRef<?>>() {
            @Override
            public <S> Computation<S, SNodeRef<?>> getInstance() {
                return model.<S>getNodeById(42).map(ref -> ref);
            }
        };

        // Now, we can run the computation to get an SNodeRef<?>
        SNodeRef<?> ref42 = Computation.run(comp2);

        // Nevertheless, we are still safe, because SNodeRef alone doesn't give us access to the SNode it refers to.
        // It only returns a Computation that, when run, has access to the underlying SNode.
        Computation<?, String> useless2 = ref42.getPresentation();
        // This computation, however, cannot be run, for the same reason as `useless1` above:
        // It is a computation for one specific context (although unknown, but already decided),
        // but we can only run universal computations.

        // The only useful thing we can do is to extract some data that are not protected
        // by the universally quantified type variable S, and are already useful by themselves (unlike SNodeRef<?>).
        UniversalComputation<String> comp3 = new UniversalComputation<String>() {
            @Override
            public <S> Computation<S, String> getInstance() {
                return model.<S>getNodeById(42).flatMap(ref -> ref.getPresentation());
            }
        };

        // We can now execute the last computation to get the result
        String presentation42 = Computation.run(comp3);
    }
}
