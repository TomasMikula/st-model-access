package model;

import util.Computation;

/**
 * Model never returns {@link SNode}, which is only for MPS internal use.
 * It only returns computations within which we have access to {@linkplain SNode}
 * via {@link SNodeRef}.
 */
public interface Model {

    /**
     * The only way for {@code SNodeRef<S>} to escape the context of the returned computation
     * is to forget its {@code S} type argument, thus resulting in {@code SNodeRef<?>}.
     * (To convince yourself of that, try to create {@code UniversalComputation<SNodeRef<T>>},
     * for any previously specified {@code T}.)
     *
     * The trick is that such {@code SNodeRef<?>}, or even {@code SNodeRef<T>} for any fixed T,
     * does not provide direct access to the underlying SNode. It only returns computations
     * (see e.g. {@link SNodeRef#getPresentation()}) that provide access to the underlying node,
     * but only from within the same computation "context" S in which the {@code SNodeRef<S>}
     * itself was created. If we have an {@code SNodeRef<?>} detached from its original computation,
     * there is no way to integrate it into a different computation, since the contexts are
     * potentially different (not <em>provably</em> the same, which is sufficient for the type system to preclude it).
     */
    <S> Computation<S, SNodeRef<S>> getNodeById(int id);
}

