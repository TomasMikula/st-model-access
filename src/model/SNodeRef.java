package model;

import util.Computation;

/**
 * A wrapper around SNode. The extra type parameter {@code S} allows it to be used
 * from a {@link util.UniversalComputation} without leaking out of that computation.
 */
public final class SNodeRef<S> {
    private final SNode node;

    SNodeRef(SNode node) {
        this.node = node;
    }

    public Computation<S, Integer> getId() { return Computation.defer(node::getId); }
    public Computation<S, String> getPresentation() { return Computation.defer(node::getPresentation); }
}
