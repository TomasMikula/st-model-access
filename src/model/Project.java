package model;

import util.Computation;

public class Project {
    public static Model getModel() {
        return new ModelImpl();
    }
}

class ModelImpl implements Model {

    @Override
    public <S> Computation<S, SNodeRef<S>> getNodeById(int id) {
        // Don't fetch a node yet: that would mean that the returned computation holds a node.
        // Instead, create a function that will be run and fetch the node later.
        return Computation.defer(() -> new SNodeRef<>(fetchNode(id)));
    }

    private SNode fetchNode(int id) {
        return new SNode(id);
    }
}