package model;

public class SNode {
    private final int id;

    SNode(int id) {
        this.id = id;
    }

    public int getId() { return id; }
    public String getPresentation() { return "node[" + id + "]"; }
}
