package util;

/**
 * If {@link Computation} is computation restricted to a specific context,
 * {@linkplain UniversalComputation} is computation runnable in <em>any</em> context.
 *
 * In Haskell, this is the type {@code forall S. ST S A}.
 *
 * @param <A> result of the computation
 */
public interface UniversalComputation<A> {

    /** For any "context" {@code S}, returns an instance of this computation runnable in that context. */
    <S> Computation<S, A> getInstance();
}
