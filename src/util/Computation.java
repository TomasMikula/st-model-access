package util;

import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Represents a computation producing {@code A}, but restricted to "context" S.
 *
 * By themselves, instances of this class are useless for the user:
 * there is no way, for the user, to run any computation described by such instance.
 * Only computations that make sense universally in all "contexts" S are runnable.
 * Such computations are represented by {@link UniversalComputation}.
 * Therefore, a method returning {@code Computation<S, A>} is only useful if S is
 * a type parameter of that method or of some enclosing class, because such methods
 * can be used to implement {@link UniversalComputation#getInstance()}.
 *
 *
 * In Haskell, this type is called {@code ST} (for "state thread").
 * @see <a href="http://hackage.haskell.org/package/base-4.11.1.0/docs/Control-Monad-ST.html">Control-Monad-ST</a>
 *
 * @param <S> Doesn't have any concrete meaning, but can be thought of as
 *           some context (e.g. thread) where this computation can be run,
 *           or where this computation makes sense at all.
 *           For example, {@code util.Computation<S, A>} may only be run in thread S.
 * @param <A> result of the computation
 */
public abstract class Computation<S, A> {

    // private constructor to prevent foreign subclasses
    private Computation() {}

    public static <S, A> Computation<S, A> done(A a) {
        return new Done<>(a);
    }

    public static <S, A> Computation<S, A> defer(Supplier<A> supplier) {
        return Computation.<S, Void>done(null).<A>map(dummy -> supplier.get());
    }

    public <B> Computation<S, B> map(Function<A, B> f) {
        return this.flatMap(a -> done(f.apply(a)));
    }

    public <B> Computation<S, B> flatMap(Function<A, Computation<S, B>> f) {
        return new FlatMap<>(this, f);
    }

    /** It is crucial that this method is not visible to the user and
     * not used outside of the {@link Computation#run(UniversalComputation)} method.
     */
    abstract A run();

    public static <A> A run(UniversalComputation<A> c) {
        // It does not matter to what "context" we instantiate the computation.
        // As noted above, the context is actually meaningless.
        // But this fact is not visible to the user, and we insist that the user provides a "universal" computation.
        Computation<?, A> comp = c.getInstance();
        return comp.run();
    }


    private static final class Done<S, A> extends Computation<S, A> {
        private final A value;

        private Done(A value) {
            this.value = value;
        }

        @Override
        A run() {
            return value;
        }
    }

    private static final class FlatMap<S, A, B> extends Computation<S, B> {
        private final Computation<S, A> ca;
        private final Function<A, Computation<S, B>> f;

        private FlatMap(Computation<S, A> ca, Function<A, Computation<S, B>> f) {
            this.ca = ca;
            this.f = f;
        }

        @Override
        B run() {
            A a = ca.run();
            Computation<S, B> cb = f.apply(a);
            return cb.run();
        }
    }
}

